#!/home/mahabharata871/djangotest/bin/python2
#! -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from optparse import OptionParser

import sys
import psycopg2


parser = OptionParser("usage: %prog [options] arg1 arg2 arg3")

parser.add_option('-c', '--campaigns', dest='campaigns', type=int, default=1, help="Number of campaigns")
parser.add_option('-p', '--products', dest='products', type=int, default=1, help="Number of products")
parser.add_option('-C', '--competitors', dest='competitors', type=int, default = 1, help="Number of competitors")

options, args = parser.parse_args()

n = options.campaigns
t = options.products
k = options.competitors

#print n, t, k


conn = psycopg2.connect("dbname='djangotest' user='root' host='localhost' password='qwerty'")
cur = conn.cursor()

i = 1
while i <= n:
    cur.execute("insert into campaigns_campaign(name, type, domain, is_active, is_scanning_enabled, is_url_rewriting_enabled) \
		values('Compaign-"+str(i)+"', '1', 'Domain-"+str(i)+".com', true, true, true);")
    j = 1
    while j <= t:
	if (i*j) == 1:
	    price = 0
	elif i == n and j == t:
	    price = 1000
	else:
	    price = 1000/n/i/j*(500/n+0.25)
	if price > 1000:
	    price = 1000

	cur.execute("insert into campaigns_campaign(name, type, domain, is_active, is_scanning_enabled, is_url_rewriting_enabled) \
		    values('Compaign-"+str(i)+"', '1', 'Domain-"+str(i)+".com', true, true, true);")

	cur.execute("insert into campaigns_product(campaign_id, url, title, price)\
		    values((select CURRVAL(pg_get_serial_sequence('campaigns_campaign','id'))), 'Campaign-"+str(i)+".com/product/"+str(j)+"', 'Product "+str(j)+"', "+str(price)+");")

	y = 1
	while y <= k:
	    cur.execute("insert into campaigns_campaign(name, type, domain, is_active, is_scanning_enabled, is_url_rewriting_enabled) \
			values('Compaign-"+str(i)+"', '1', 'Domain-"+str(i)+".com', true, true, true);")

	    cur.execute("insert into campaigns_product(campaign_id, url, title, price)\
			values((select CURRVAL(pg_get_serial_sequence('campaigns_campaign','id'))), 'Campaign-"+str(i)+".com/product/"+str(j)+"', 'Product "+str(j)+"', "+str(price)+");")

	    cur.execute("insert into campaigns_competitor(campaign_id, name, domain)\
			values((SELECT setval((select CURRVAL(pg_get_serial_sequence('campaigns_campaign','id'))), 'Competitor-"+str(y)+"', 'example-"+str(y)+".com');")

	    cur.execute("insert into campaigns_matching(product_id, competitor_id, url, price)\
			values((select CURRVAL(pg_get_serial_sequence('campaigns_product','id'))), (select CURRVAL(pg_get_serial_sequence('campaigns_competitor','id'))),\
			 'example-"+str(y)+".com/product/"+str(y)+"', "+str(price)+");")
		
	    print "Compaign-"+str(i)+"', '1', 'Domain-"+str(i)+".com', true, true, true);"
#		print "Compaign-"+str(i)+"', '1', 'Domain-"+str(i)+".com', true, true, true);"
#		print ""

	    y += 1
	j += 1
    i +=1

#print 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'




class Command(NoArgsCommand):
    def bla():
	pass